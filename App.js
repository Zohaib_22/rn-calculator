

import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Button
} from 'react-native';



const App = (props) => {
  let [result , setResult] = useState(0);
  let [num1 , setNum1] = useState('');
  let[num2 , setNum2] = useState('');
  return (
     <>
<View style={styles.container}>
          <TextInput style={styles.input}  autoCapitalize="none" keyboardType="number-pad" autoFocus={true} value={num1} onChangeText={val => setNum1(val)}/>
          <TextInput style={styles.input}  autoCapitalize="none" keyboardType="number-pad" autoFocus={false} value={num2} onChangeText={val => setNum2(val)}/>
        <View>
          <Button color='red' title="+" onPress={()=>{
            const val1 = parseFloat(num1)
            const val2 = parseFloat(num2)
            setResult(val1+val2)
           }}   />
          <Button color='green' title="-" onPress={()=>{
            const val1 = parseFloat(num1)
            const val2 = parseFloat(num2)
            setResult(val1-val2)
          }}  />
          <Button color='blue' title="*" onPress={()=>{
            const val1 = parseFloat(num1)
            const val2 = parseFloat(num2)
            setResult(val1*val2)
          }}  />
          <Button color='yellow' title="/" onPress={()=>{
            const val1 = parseFloat(num1)
            const val2 = parseFloat(num2)
            if(val2 <=0)
            {
              setResult(0)
            }
            else
            {
              setResult(val1/val2)
            }
            
          }}  />
        </View>
      <View>
        <Text style={styles.text}>
         Result: {result}
        </Text>
      </View>
</View>
      </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  btnContainer:{
    flexDirection: 'row',
    width: '80%',
    justifyContent: 'space-around', 
    alignItems: 'center',
  },
  input:
  {
    width: '90%',
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    padding: 5,
    marginVertical: 10,
    fontSize: 22,  
  },
  text:
  {
    fontSize: 25,
    justifyContent : 'center',
  },
});

export default App;
